import './App.css';
import React, {useState, useEffect, useRef, useContext} from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom"
import TopBar from "./components/TopBar"
//Views Import
import HomePage from "./views/HomePage/HomePage"
import Sales from "./views/Sales/Sales"
import Materials from "./views/Materials/Materials"
import Products from "./views/Products/Products"
import Logs from "./views/Logs/Logs"
import EditProducts from './views/Products/EditProducts';
import EditMaterials from './views/Materials/EditMaterials';
import EditStocks from './views/Stocks/EditStocks';
import EditSales from './views/Sales/EditSales';
import Context from "./store/context"
import Stocks from './views/Stocks/Stocks';
import Axios from "axios"
function App() {

  
  const {state, actions} = useContext(Context);

  const storeMessages = (list)=>{
    actions({type:"setState", payload:{...state, messages:list, messagesQuantity:list.length}})
  }

  let messageList=[];
  const messageAnalizer = () => {
    Axios.get("http://localhost:3001/search/stocks").then((response)=>{
      if(response.data.length>0){
        response.data.forEach(item=>{
          if(item.quantity == 0){
            messageList.push({name: (item.materialName!=null ? item.materialName : item.productName), type:"stockWarning"})
          }else if (item.quantity < 0){
            messageList.push({name: (item.materialName!=null ? item.materialName : item.productName), type:"stockError"})
          }
        })
        storeMessages(messageList);
      }
    });

  }
  setTimeout(() => {
    messageAnalizer();
  }, 2000);
  useEffect(() => {
    messageAnalizer();
  }, [])
  return (
  
    <div className="App">
        
      <div className="topBar">
        <TopBar > </TopBar> 
      </div>
      
      <Router>
        <Route path="/" exact render={ () => <HomePage />}></Route>
        <Route path="/sales" render={(props) => <Sales />}></Route>
        <Route path="/salesEdition/:id" render={(props) => <EditSales />}></Route>
        <Route path="/products" render={(props) => <Products />}></Route>
        <Route path="/productsEdition/:id" render={(props) => <EditProducts/>}></Route>
        <Route path="/materials" render={(props) => <Materials />}></Route>
        <Route path="/materialsEdition/:id" render={(props) => <EditMaterials/>}></Route>
        <Route path="/stocks" render={(props) => <Stocks />}></Route>
        <Route path="/EditStocks/:type" render={(props) => <EditStocks/>}></Route>
        <Route path="/logs" render={(props) => <Logs />}></Route>
      </Router>


        <div></div>
    </div>
  );
}

export default App;
