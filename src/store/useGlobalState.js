import {useState} from 'react'

const useGlobalState = () => {
    const [state, setState] = useState({value: "", messages:[], messagesQuantity:0, changeCheck:0});
    
    const actions = (action) => {
        const {type, payload} = action;
        switch (type) {
            case "setState":
                return setState(payload);
            default:
                return state;
        }
    }
    
    return {state, actions};
}

export default useGlobalState;