
import React, { useState, useEffect } from 'react'
import "../../Admin.css"
import { withStyles } from '@material-ui/core/styles';
import { purple } from '@material-ui/core/colors';
import Axios from 'axios'
import TableForm from "../../components/StockTable.js"
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import CustomButton from "../../components/CustomButton"

const TextFieldCustom = withStyles((theme) => ({
  root: {
    backgroundColor: purple[0],
    '&:hover': {
      
    },
    '& label.Mui-focused': {
      color: purple[600],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: purple[800],
    },
  },
}))(TextField);


const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));
export default function Stocks() {
  
  useEffect(() => {
    Axios.get("http://localhost:3001/search/materials").then((response)=>{
      Axios.get("http://localhost:3001/search/stocks").then((responseS)=>{
        responseS.data.forEach(element => {
          response.data.forEach(item=>{
            if(element.materialName===item.name){
              item.stock=element.quantity;
              item.invested=item.cost*element.quantity;
            }
          })
        });
        setMaterialList(response.data);
        setFullList(response.data);;  
      })
    })
    Axios.get("http://localhost:3001/search/products").then((response)=>{
      Axios.get("http://localhost:3001/search/stocks").then((responseS)=>{
        responseS.data.forEach(element => {
          response.data.forEach(item=>{
            if(element.productName===item.name){
              item.stock=element.quantity;
              item.invested=item.price*element.quantity;
            }
          })
        });
        setProductList(response.data);
        setFullListP(response.data);;  
      })
    })
  }, []);
  
  const titleAdmin = "Administración de Stocks";
  const [fullList, setFullList] = useState([]);
  const [fullListP, setFullListP] = useState([]);
  const [materialList, setMaterialList] = useState([]);
  const tableColumns = [
    {label:"Nombre", id:"name",  minWidth: 120 },
    {label:"Costo", id:"cost", minWidth: 100},
    {label:"Cantidad de Unidades en Stock", id:"stock", minWidth:200},
    {label:"Capital invertido", id:"invested", minWidth: 100},
  ];
  const [productList, setProductList] = useState([]);
  const tableColumnsP = [
    {label:"Nombre", id:"name",  minWidth: 120 },
    {label:"Precio", id:"price", minWidth: 100},
    {label:"Cantidad de Unidades en Stock", id:"stock", minWidth:200},
    {label:"Valor Neto despues de Venta", id:"invested", minWidth: 100},
  ];
  const classes = useStyles();
  return (
    
        <form noValidate autoComplete="off">
        <div className="adminMain">
          <h1 className="titleAdmin"> {titleAdmin}</h1>
          <hr className="titleHR"></hr>
          <h4 className="titleAdmin">Materiales</h4>
          <div className="titleHR"></div>
          <TextFieldCustom className="inputField" id="standard-basic" label="Nombre" onChange={(event)=>{
            setMaterialList( event.target.value=== "" ? fullList : fullList.filter(item => item.name.toLowerCase().includes(event.target.value.toLowerCase())))
          }}/>
          <div className="tableSpan">
          <TableForm tableData={materialList} tableColumns={tableColumns} type="materials"> </TableForm>
          </div>
          <CustomButton className="addButton" href="/EditStocks/materials" variant="outlined" color="default" size="large">Modificar Stock</CustomButton>  
          <hr className="titleHR"></hr>
          <h4 className="titleAdmin">Productos</h4>
          <div className="titleHR"></div>
          <TextFieldCustom className="inputField" id="standard-basic" label="Nombre" onChange={(event)=>{
            setProductList( event.target.value=== "" ? fullListP : fullListP.filter(item => item.name.toLowerCase().includes(event.target.value.toLowerCase())))
          }}/>
          <div className="tableSpan">
          <TableForm tableData={productList} tableColumns={tableColumnsP} type="products"> </TableForm>
          </div>

          <CustomButton className="addButton" href="/EditStocks/products" variant="outlined" color="default" size="large">Modificar Stock</CustomButton>          
        </div>
        </form>
    )
}