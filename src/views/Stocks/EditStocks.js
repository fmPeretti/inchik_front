import React, {useState, useEffect} from 'react'; 
import "./Stocks.css"
import Axios from "axios"
import InputLabel from '@material-ui/core/InputLabel';
import { useParams } from 'react-router-dom';
import CustomButton from '../../components/CustomButton';
import CancelButton from '../../components/CancelButton';
import "../../Admin.css"
import CustomCard from '../../components/CustomCard';
import Select from "@material-ui/core/Select";
import Chip from "@material-ui/core/Chip";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import InputChip from "../../components/InputChip"
import { useHistory } from 'react-router';

const useStylesCards = makeStyles({
  root: {
    minWidth: 300,
    minHeight: 150,
    background: '#e06ab1',
    gridColumn:"1/11",
  },
  title: {
    color: "white",
  },
});
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300
  },
  chips: {
    display: "flex",
    flexWrap: "wrap"
  },
  chip: {
    margin: 2
  },
  noLabel: {
    marginTop: theme.spacing(3)
  }
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};


  
  
export default function EditStocksMaterials() {
  const [saveOgQuantity, setSaveOgQuantity] = useState([]);
  const history = useHistory();
  const { type } = useParams();
  const getListAction = (pId) =>{
    var dummyList=[];
    for(var i=0; i<materialList.length; i++){
      if( materialList[i].id!=pId ){
        dummyList.push(materialList[i]);
      } 
    }
    setMaterialList(dummyList);
  }
  const classes = useStyles();
  const classesCard = useStylesCards();
  const handelQuantityChange = (value, mId)=>{
    var dummyMaterial={};
    var dummyList=[];
    for(var i=0; i<materialList.length; i++){
      if( materialList[i].id==mId ){
        dummyMaterial=materialList[i];
        dummyMaterial.quantity=value;
        dummyList.push(dummyMaterial);
      }else{
        dummyList.push(materialList[i]);
      }
    }
    setMaterialList(dummyList);
  };
  const handleChange = (event) => {
    setMaterialList(event.target.value);
  };
  const searchForStocks = () =>{
    Axios.get("http://localhost:3001/search/stocks").then((response)=>{
      if(type!="materials") {
        setMaterialStocks(JSON.parse(JSON.stringify(response.data.filter(item => type!="materials" ? item.productName==null : item.productName!=null ))));
        Axios.get("http://localhost:3001/search/products").then((response2)=>{
          response2.data.forEach((item)=>{
            response.data.forEach((itemS)=>{
              if(item.name==itemS.productName){
                itemS.recipe=JSON.parse(item.recipe);
              }
            })
          })
        })
      }
      setFullList(response.data.filter(item => type=="materials" ? item.productName==null : item.productName!=null ));
      setSaveOgQuantity(JSON.parse(JSON.stringify(response.data.filter(item => item.productName!=null))));
    });
  }
  useEffect(() => {
    searchForStocks();
  }, []);
  const [materialStocks, setMaterialStocks] = useState([]);
  const update = ()=>{
    let stockCheckErr=[];
    let quantityDiff;
    if(materialList!=null && materialList.length!=0){
      if(type=="products"){
        materialList.forEach((prod)=>{
          quantityDiff=0;
          if(stockCheckErr.indexOf(prod.productName) == -1){
            saveOgQuantity.forEach((og)=>{
              if(og.productName==prod.productName){
                quantityDiff= prod.quantity - og.quantity;
              }
            })
            if(quantityDiff>0){
              prod.recipe.forEach((matR)=>{
                  materialStocks.forEach((mat)=>{
                      if(mat.materialName==matR.name && stockCheckErr.indexOf(prod.productName) == -1){
                        if(matR.quantity*quantityDiff > mat.quantity){
                          stockCheckErr.push(prod.productName);
                        }else{
                          mat.quantity = mat.quantity - matR.quantity*quantityDiff;
                        }
                      }
                    })
              })
            }  
          }
        })

      };
      if(type=="products" && stockCheckErr.length==0 ){
        Axios.post("http://localhost:3001/update/stocks", materialStocks);
      }
      if(stockCheckErr.length==0 || type=="materials"){
        Axios.post("http://localhost:3001/update/stocks", materialList).then(response=>{
          if(response.statusText==="OK"){
            alert("El Stock se actualizó correctamente");  
            history.push("/stocks");
          }else {
            alert("El Stock no se Pudo actualizar por el Error:  " + response.data.statusText);
          }
  
        });
      }else{
        alert("No se cuenta con Stock Para Ingresar estos Productos:" + JSON.stringify(stockCheckErr));
      }
    }else{
      alert("Debe seleccionar porlomenos 1 "+ (type=="materials" ? "material" : "producto") + " para continuar");
    }
  }
  const [fullList, setFullList] = useState([]);
  const [materialList, setMaterialList] = React.useState([]);
  const [recipe, setRecipe] = useState("");



    return (
      <div>
        <div className="productsEdition" >
          <hr className = "titleHR"></hr>
          <div className="inputsEdition">
            <InputLabel >Selecciona { type=="materials" ? "Materiales": "Productos"} para modificar Stocks</InputLabel>
            <Select
              multiple
              value={materialList}
              onChange={handleChange}
              input={<InputChip/>}
              renderValue={(selected) => (
                <div className={classes.chips}>
                  
                  {selected.map((value) =>
                    <Chip key={value.id} label={ type=="products" ? value.productName : value.materialName} className={classes.chip} />
                  )}
                </div>
              )}
              MenuProps={MenuProps}
            >
              {fullList.map((name) => (
                <MenuItem
                  key={name.id}
                  value={name}
                >
                  { type=="products" ? name.productName : name.materialName}
                </MenuItem>
              ))}
            </Select>
          </div>
          <div className = "titleHR"></div>
        {materialList.map((item)=>{
          return <CustomCard quantity={item.quantity} handelQuantityChange={handelQuantityChange} title={type=="products" ? item.productName : item.materialName} description={"Cambie el Valor de la Casilla al valor TOTAL de unidades de " + (type=="products" ? item.productName : item.materialName) + " en Stock... es decir, si quiere agregar 3 unidades al stock, sume 3 al valor previo de la casilla"} pId={item.id} handleMaterialChange={getListAction}></CustomCard>
        })} 

        <div className="dummySpaceForButton"></div>
        <CancelButton color="secondary" size="large" className="cancelButton" onClick={()=>{
         history.push("/Stocks");
        }}> Cancelar </CancelButton>
        <CustomButton className="saveButton" variant="outlined" color="default" size="large" onClick={()=>{
          update()
        }}> Guardar </CustomButton>
        </div>
      </div>
    )
}
