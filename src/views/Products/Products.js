
import React, { useState, useEffect } from 'react'
import "../../Admin.css"
import { withStyles } from '@material-ui/core/styles';
import { purple } from '@material-ui/core/colors';
import Axios from 'axios'
import TableForm from "../../components/TableForm.js"
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import CustomButton from "../../components/CustomButton";


const TextFieldCustom = withStyles((theme) => ({
  root: {
    backgroundColor: purple[0],
    '&:hover': {
      
    },
    '& label.Mui-focused': {
      color: purple[600],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: purple[800],
    },
  },
}))(TextField);

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));
export default function Products() {
  
  const searchAndBuildProducts = () =>{
    Axios.get("http://localhost:3001/search/materials").then((response)=>{
      Axios.get("http://localhost:3001/search/products").then((responseP)=>{
        responseP.data.forEach((item)=>{
          item.recipe = JSON.parse(item.recipe);  
          item.cost=0;
          item.recipe.forEach((mat=>{
            response.data.forEach((oMat=>{
              if(mat.name===oMat.name) mat.cost = oMat.cost;
            }))
            item.cost = (mat.quantity * mat.cost) + item.cost;
          }))
        })
        setFullList(responseP.data);
        setProductList(responseP.data);
      })

    })
  }

   useEffect( async () => {
    searchAndBuildProducts();
  }, []);
  
  const titleAdmin = "Administración de Productos";
  const [fullList, setFullList] = useState([]);
  const [productList, setProductList] = useState([]);
  const tableColumns = [
    {label:"Id de Producto", id:"id",  minWidth: 40 },
    {label:"Nombre", id:"name",  minWidth: 120 },
    {label:"Precio", id:"price", minWidth: 100},
    {label:"Costo", id:"cost", minWidth: 40 }
  ];
  const classes = useStyles();
  return (
    
        <form noValidate autoComplete="off">
        <div className="adminMain">
          <h1 className="titleAdmin"> {titleAdmin}</h1>
          <hr className="titleHR"></hr>
          <TextFieldCustom className="inputField" id="standard-basic" label="Nombre" onChange={(event)=>{
            setProductList( event.target.value=== "" ? fullList : fullList.filter(item => item.name.toLowerCase().includes(event.target.value.toLowerCase())))
          }}/>
          <div className="tableSpan">
          <TableForm tableData={productList} tableColumns={tableColumns} type="products"> </TableForm>
          </div>

          <CustomButton className="addButton" href="/productsEdition/0" variant="outlined" color="default" size="large"> Agregar +</CustomButton>          
        </div>
        </form>
    )
}