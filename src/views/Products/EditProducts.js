import React, {useState, useEffect} from 'react'; 
import "./Products.css"
import Axios from "axios"
import InputEdition from "../../components/InputEdition"
import InputLabel from '@material-ui/core/InputLabel';
import { useParams } from 'react-router-dom';
import CancelButton from '../../components/CancelButton';
import CustomButton from '../../components/CustomButton';
import "../../Admin.css"
import CustomCard from '../../components/CustomCard';
import CustomCardRecipe from '../../components/CustomCardRecipe';
import Select from "@material-ui/core/Select";
import Chip from "@material-ui/core/Chip";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import InputChip from "../../components/InputChip"
import { useHistory } from 'react-router';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const useStylesCards = makeStyles({
  root: {
    minWidth: 300,
    minHeight: 150,
    background: '#e06ab1',
    gridColumn:"1/11",
  },
  title: {
    color: "white",
  },
});
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300
  },
  chips: {
    display: "flex",
    flexWrap: "wrap"
  },
  chip: {
    margin: 2
  },
  noLabel: {
    marginTop: theme.spacing(3)
  }
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};


  
  
export default function EditProducts() {
  const history = useHistory();
  const getListAction = (pId) =>{
    var dummyList=[];
    for(var i=0; i<materialList.length; i++){
      if( materialList[i].id!=pId ){
        dummyList.push(materialList[i]);
      } 
    }
    setMaterialList(dummyList);
    setProduct({...product, recipe: dummyList});
  }
  const classes = useStyles();
  const classesCard = useStylesCards();
  const handelQuantityChange = (value, mId)=>{
    var dummyMaterial={};
    var dummyList=[];
    for(var i=0; i<materialList.length; i++){
      if( materialList[i].id==mId ){
        dummyMaterial=materialList[i];
        dummyMaterial.quantity=value;
        dummyList.push(dummyMaterial);
      }else{
        dummyList.push(materialList[i]);
      }
    }
    setMaterialList(dummyList);
    setProduct({...product, recipe: dummyList});
  };
  const handleChange = (event) => {
    setMaterialList(event.target.value);
  };
  const searchForMaterials = () =>{
    Axios.get("http://localhost:3001/search/materials").then((response)=>{
      setFullList(response.data);
    });
  }
  useEffect(() => {
    
    searchForMaterials();

    if(id!=0){
      Axios.get("http://localhost:3001/retrieveProductById", {params:{id:id}}).then((response)=>{
        if(response.data[0]!=undefined){
           response.data[0].recipe = JSON.parse(response.data[0].recipe);
           
           if(response.data[0].recipe.length===0){
            setChangeRecipe(true);
           }
           setOldRecipe(response.data[0].recipe); 
           setProduct(response.data[0]);
          }
      })
    }else{
      setChangeRecipe(true);
      setProduct({});
      setPrice(null);
      setName(null);
      setRecipe(null);
      setDescription(null);
    }
  }, []);
  const save = ()=>{
    if(name!=null && name.length>0 && price>0 && firstStock!=null && materialList!=null && materialList.length>0){
      Axios.post("http://localhost:3001/save/product", {price:price, name:name, recipe:materialList, description:description, firstStock: firstStock}).
        then((response)=>{
          if(response.data.statusText==="OK" || response.data.statusText==undefined){
            alert("El Producto se agregó Correctamente");
            history.push("/products")
          } 
          else alert("El Producto no se Pudo agregar por el Error:  " + response.data.statusText);
        })
    }else alert("Campos incompletos");
  }
  const update = ()=>{
    if(product.name.length>0 && product.price>0){
      if(changeRecipe){
        setProduct({...product, recipe: materialList});
      }
      Axios.post("http://localhost:3001/update/product", product).
        then((response)=>{
          if(response.statusText==="OK"){
            alert("El Producto se guardó correctamente");  
            history.push("/products");
          }else  alert("El Producto no se Pudo agregar por el Error:  " + response.data.statusText);
        })
    }else alert("Campos incompletos");
}
  const [fullList, setFullList] = useState([]);
  const [materialList, setMaterialList] = React.useState([]);
  const [product, setProduct] = useState({});
  const [oldRecipe, setOldRecipe] =  React.useState([]);
  const { id } = useParams();
  const [name, setName] = useState("hola");
  const [description, setDescription] = useState("");
  const [recipe, setRecipe] = useState("");
  const [changeRecipe, setChangeRecipe] = useState(false);
  const [price, setPrice] = useState(0);
  const [request, setRequest] = useState({name:"", description:"", recipeId:0, price:0});
  const [firstStock, setFirstStock] = useState();
  function defaultStock(){
    if(id==0){
      return(
        <div className = "inputsEdition">
          <InputLabel> Stock Inicial </InputLabel>
          <InputEdition  className="inputTextBox" inputProps={{placeholder: "Stock Inicial"}} onChange={(event)=>{
                setFirstStock(event.target.value);
            }}></InputEdition>
        </div>
      )
    }
  };
  function RecipeOld(){
    if(!changeRecipe){
    return(
      <Card className={classesCard.root} variant="outlined">
      <CardHeader className={classesCard.title}
      action={
        <IconButton className={classesCard.title} aria-label="settings" onClick={()=>{setChangeRecipe(true); setProduct({... product, recipe:[]})}}>
          <CloseIcon />
        </IconButton>
      }
      title="Receta"
      />
      <CardContent>
          {oldRecipe.map((item)=>{
            return <CustomCardRecipe title={item.name} description={item.description} quantity={item.quantity}></CustomCardRecipe>
          })}
      </CardContent>
      </Card>
    )
    }else{
      return <div></div>;
    }
  }
    return (
      <div>
        <div className="productsEdition" >
          <div className = "inputsEdition">
            <InputLabel> Nombre </InputLabel>
            <InputEdition  disabled={id!=0} value={product.name} className="inputTextBox" inputProps={{placeholder: "Nombre"}} onChange={(event)=>{
                  setName(event.target.value);
                  setProduct({...product, name: event.target.value});
              }}></InputEdition>
          </div>
          <div className = "inputsEdition">
            <InputLabel> Precio </InputLabel>
            <InputEdition value={product.price} className="inputTextBox" inputProps={{placeholder: "Precio", type: "number"}} onChange={(event)=>{
              setPrice(event.target.value);
              setProduct({...product, price: event.target.value});
            }} />
          </div>
          <div className = "textAreaEdition">
          <InputLabel> Descripción </InputLabel>
          <InputEdition value={product.description} className="inputTextArea" inputProps={{placeholder: "Descripción"}} onChange={(event)=>{
            setDescription(event.target.value);
            setProduct({...product, description: event.target.value});
          }} />
          </div>
          {defaultStock()}
          <hr className = "titleHR"></hr>
          <div className="inputsEdition">
            <InputLabel >Selección de Materiales para Receta</InputLabel>
            <Select
              inputProps={{disabled:!changeRecipe}}
              multiple
              value={materialList}
              onChange={handleChange}
              input={<InputChip/>}
              renderValue={(selected) => (
                <div className={classes.chips}>
                  
                  {selected.map((value) =>
                    <Chip key={value.id} label={value.name} className={classes.chip} />
                  )}
                </div>
              )}
              MenuProps={MenuProps}
            >
              {fullList.map((name) => (
                <MenuItem
                  key={name.id}
                  value={name}
                >
                  {name.name}
                </MenuItem>
              ))}
            </Select>
          </div>
          <div className = "titleHR"></div>
        {materialList.map((item)=>{
          return <CustomCard quantity={item.quantity} handelQuantityChange={handelQuantityChange} title={item.name} pId={item.id} description={item.description} handleMaterialChange={getListAction}></CustomCard>
        })} 
        
        <RecipeOld></RecipeOld>

        <div className="dummySpaceForButton"></div>
        <CancelButton color="secondary" size="large" className="cancelButton" onClick={()=>{
         history.push("/Products");
        }}> Cancelar </CancelButton>
        <CustomButton className="saveButton" variant="outlined" color="default" size="large" onClick={()=>{
         id==0 ? save() : update();
        }}> Guardar </CustomButton>
        </div>
      </div>
    )
}
