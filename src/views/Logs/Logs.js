import React, {useContext} from 'react';
import { Alert, AlertTitle } from '@material-ui/lab';
import "./Logs.css"
import Context from "../../store/context"

export default function Logs() {
  const {state} = useContext(Context);
  return (
    <div className="logsDiv">

      {state.messages.map( item =>{
        return (
          <Alert severity={item.type=="stockWarning" ? "warning" : "error"} className="logsAlert">
            <AlertTitle>{ item.type=="stockWarning" ? "Alerta" : "Error"}</AlertTitle>
              { item.type=="stockWarning" ? "Se acabó el Stock de: " : "El Stock ingresado es Negativo: "} {item.name} — <strong>{ item.type=="stockWarning" ? "Agregar Stock!" : "Modificar a un Valor Positivo"} </strong>
          </Alert>
        )
      })}
    </div>
  );
}