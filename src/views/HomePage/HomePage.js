
import React from 'react'
import { Route } from 'react-router-dom'
import "./HomePage.css"
import Paper from '@material-ui/core/Paper';


export default function HomePage() {
    return (
      <div className="homePage">

      <Paper elevation={3} className="homePageBox">
        
      <h1>Bienvenidos al Sistema de Administración INCHIK</h1>
        <h3> ¿Como Empezar?</h3>
        <ol>  
        <li>Es Conveniente Comenzar Registrando toda la lista de materiales que pueden ser usados para la producción de cualquiera de los productos.</li>  
        <li>Luego de tener todos los materiales necesarios registrados, Podemos proceder a Registrar todos los Productos.</li>
        <li>Con los primeros 2 pasos listos, Ahora solo queda Confirmar que toda la información introducida sea la correcta, y luego verificar que los stocks Coincidan con la realidad.</li>
        <li>Si notamos alguna discrepancia entre el stock real y el que el sistema registrado, diríjase a la pestaña de Stocks para modificarlo y corregir todo lo necesario.</li>
        <li>Con esos 4 pasos Listos, ya podemos empezar a registrar Ventas, y utilizar el resto del sistema de forma normal.</li>
        <li>Cualquier registro no deseado puede ser eliminado desde la pestaña de administración respectiva al tipo de registro.</li>
        </ol>  

        <h3> Cosas a Tener en Cuenta</h3>
        <ol> 
        <li>No es Conveniente Eliminar materiales que estén involucrados en la receta de algun Producto.</li>  
        <li>Al registrar un Producto, se le registra una Receta que no es editable, si se quiere cambiarle la receta al producto eventualmente, habrá que Descartar la Receta vieja y Crear una Receta nueva para el producto desde 0 (Desde la edición del Producto).</li>
        <li>Las Ventas Registradas Toman automáticamente el stock de los productos venidos, Si se comete una Equivocación al registrar una venta y quiere eliminar ese registro, debe tener en cuenta que tiene que volver a agregar las unidades al stock de los productos de la venta anterior.</li>
        <li>Similar que con el punto anterior, al registrar Stock de los Productos, el Stock de los materiales se ve automáticamente afectado, por lo que si cometemos una equivocación y agregamos mas de los que queremos, tenemos que recordar agregar nuevamente el stock de los materiales que estuvieron involucrados.</li>
        <li>Al momento de registrar productos materiales ventas etc... se va a encontrar con campos obligatorios, si no está seguro puede intentar guardar, el sistema le notificará si hay campos faltantes, o si hubo algun error relacionado.</li>
        <li>Se cuenta con una pequeña casilla para filtrar en todas las pestañas de administración para facilitar la búsqueda en cada una de las tablas. (Esta misma no es sensible a las mayúsculas pero si a las tildes!)</li>
        <li>Los Mensajes se eliminarán solo una vez hayan sido resueltos!!</li>
        </ol> 
        <br></br>
        <br></br>
        <p> Developed by Francisco Peretti </p>
      </Paper>
        
      </div>  
    )
}
