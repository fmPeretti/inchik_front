

import React, { useState, useEffect } from 'react'
import "../../Admin.css"
import { withStyles } from '@material-ui/core/styles';
import { purple } from '@material-ui/core/colors';
import Axios from 'axios'
import TableForm from "../../components/TableForm.js"
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import CustomButton from "../../components/CustomButton";
import "./Sales.css"

const TextFieldCustom = withStyles((theme) => ({
  root: {
    backgroundColor: purple[0],
    '&:hover': {
      
    },
    '& label.Mui-focused': {
      color: purple[600],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: purple[800],
    },
  },
}))(TextField);

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function Sales() {
    const searchSales = () =>{
      Axios.get("http://localhost:3001/search/sales").then((response)=>{
          for(var i=0; i<response.data.length; i++){
            response.data[i].userInfo = JSON.parse(response.data[i].userInfo);
            if(response.data[i].userInfo!=null){  
              response.data[i].name=response.data[i].userInfo.name;
              response.data[i].surname=response.data[i].userInfo.surname;
              response.data[i].email=response.data[i].userInfo.email;
              response.data[i].phone=response.data[i].userInfo.phone;
            }
          }
          setFullList(response.data);
          setSaleList(response.data);
      })
    }
    
     useEffect( async () => {
        searchSales();
    }, []);
    
    const titleAdmin = "Administración de Ventas";
    const [fullList, setFullList] = useState([]);
    const [saleList, setSaleList] = useState([]);
    const tableColumns = [
      {label:"Id de Venta", id:"id",  minWidth: 40 },
      {label:"Fecha", id:"date",  minWidth: 120 },
      {label:"Costo", id:"cost", minWidth: 40 },
      {label:"Ganancia", id:"profit", minWidth: 100},
      {label:"Nombre del Comprador", id:"name", minWidth: 100},
      {label:"Mail del Comprador", id:"email", minWidth: 100},
    ];
    const classes = useStyles();
    
  return (
    
        <form noValidate autoComplete="off">
        <div className="adminMain">
          <h1 className="titleAdmin"> {titleAdmin}</h1>
          <hr className="titleHR"></hr>
          <label className="titleHR">Fecha Hasta</label>
          <TextFieldCustom clereable="true" type="date" className="inputField" id="standard" onChange={(event)=>{
            setSaleList( event.target.value=== "" ? fullList : fullList.filter(item => item.date<event.target.value))
          }}/>
          <div className="tableSpan">
          <TableForm tableData={saleList} tableColumns={tableColumns} type="sales"> </TableForm>
          </div>

          <CustomButton className="addButton" href="/salesEdition/0" variant="outlined" color="default" size="large"> Agregar +</CustomButton>          
        </div>
        </form>

    )
}