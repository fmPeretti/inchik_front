import React, {useState, useEffect} from 'react'; 
import "./Sales.css"
import Axios from "axios"
import InputEdition from "../../components/InputEdition"
import InputLabel from '@material-ui/core/InputLabel';
import { useParams } from 'react-router-dom';
import CustomButton from '../../components/CustomButton';
import CancelButton from '../../components/CancelButton';
import "../../Admin.css"
import CustomCard from '../../components/CustomCard';
import CustomCardRecipe from '../../components/CustomCardRecipe';
import Select from "@material-ui/core/Select";
import Chip from "@material-ui/core/Chip";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import InputChip from "../../components/InputChip"
import { useHistory } from 'react-router';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const useStylesCards = makeStyles({
  root: {
    minWidth: 300,
    minHeight: 150,
    background: '#e06ab1',
    gridColumn:"1/11",
  },
  title: {
    color: "white",
  },
});
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300
  },
  chips: {
    display: "flex",
    flexWrap: "wrap"
  },
  chip: {
    margin: 2
  },
  noLabel: {
    marginTop: theme.spacing(3)
  }
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};


  
  
export default function EditSales() {
  const history = useHistory();
  const getListAction = (pId) =>{
    var dummyList=[];
    for(var i=0; i<productList.length; i++){
      if( productList[i].id!=pId ){
        dummyList.push(productList[i]);
      } 
    }
    setProductList(dummyList);
    setSale({...sale, cart: dummyList});
  }
  const classes = useStyles();
  const classesCard = useStylesCards();
  const handelQuantityChange = (value, mId)=>{
    var dummyMaterial={};
    var dummyList=[];
    for(var i=0; i<productList.length; i++){
      if( productList[i].id==mId ){
        dummyMaterial=productList[i];
        dummyMaterial.quantity=value;
        dummyList.push(dummyMaterial);
      }else{
        dummyList.push(productList[i]);
      }
    }
    setProductList(dummyList);
    setSale({...sale, cart: dummyList});
  };
  const handleChange = (event) => {
    setProductList(event.target.value);
  };
  const searchForProductsAndMaterials = () =>{
    Axios.get("http://localhost:3001/search/products").then((response)=>{
      setFullList(response.data);
    });
    Axios.get("http://localhost:3001/search/materials").then((response)=>{
      setFullListM(response.data);
    });
    Axios.get("http://localhost:3001/search/stocks").then((response)=>{
      setFullListStocks(response.data);
    });
  }
  useEffect(() => {
    if(id==0) searchForProductsAndMaterials();

    if(id!=0){
      Axios.get("http://localhost:3001/retrieveSaleById", {params:{id:id}}).then((response)=>{
        if(response.data[0]!=undefined){
           response.data[0].cart = JSON.parse(response.data[0].cart);
           response.data[0].userInfo = JSON.parse(response.data[0].userInfo)==null ? {} : JSON.parse(response.data[0].userInfo);
           setOldCart(response.data[0].cart); 
           setSale(response.data[0]);
          }
      })
    }else{
      setChangeCart(true);
      setSale({userInfo:{}});
      setDate(null);
      setDescription(null);
      setUserInfo(null);
    }
  }, []);
  const save = ()=>{
    let noStockerr="No tienes Stock Suficiente de ";
    productList.forEach((item)=>{
      fullListStocks.forEach((stock)=>{
        if(item.name==stock.productName){
          if(item.quantity>stock.quantity){
            noStockerr=noStockerr + " " + item.name 
          }
        }
      })
    })
    if(noStockerr==="No tienes Stock Suficiente de "){
      let cost=0;
      let profit;
      let price=0;
      productList.forEach((prod)=>{
        price=price + prod.quantity*prod.price;
        JSON.parse(prod.recipe).forEach((item)=>{
          fullListM.forEach((mat)=>{
            if(mat.name === item.name){
                cost = cost + mat.cost*item.quantity*prod.quantity;
            }
          })
        })
      })
      profit=price-cost;
      if(date!=null && productList!=null && productList.length>0){
        Axios.post("http://localhost:3001/save/sales", {date:date, cart:productList, cost:cost, profit:profit, description:description, userInfo:userInfo}).
          then((response)=>{
            if(response.data.statusText==="OK" || response.data.statusText==undefined){
              alert("La Venta se agregó Correctamente");
              history.push("/sales")
            } 
            else alert("La Venta no se Pudo agregar por el Error:  " + response.data.statusText);
          })
      }else alert("Campos incompletos");
    }else{
      noStockerr = noStockerr + " Para Generar esta venta"
      alert(noStockerr);
    }
  }

  const [fullList, setFullList] = useState([]);
  const [fullListM, setFullListM] = useState([]);
  const [fullListStocks, setFullListStocks] = useState([]);
  const [productList, setProductList] = React.useState([]);
  const [sale, setSale] = useState({userInfo:{}});
  const [oldCart, setOldCart] =  React.useState([]);
  const { id } = useParams();
  const [description, setDescription] = useState("");
  const [changeCart, setChangeCart] = useState(false);
  const [date, setDate] = useState(0);
  const [userInfo, setUserInfo] = useState();
  function dateInput(){
    if(id==0){
      return(
      <div className = "textAreaEdition">
      <InputLabel> Fecha </InputLabel>
      <InputEdition className="inputTextArea" inputProps={{disabled:id!=0, placeholder: "Fecha", type: "date"}} onChange={(event)=>{
          setDate(event.target.value);
      }} />
    </div>
      )
    }else{
      return( 
      <div className = "textAreaEdition">
      <InputLabel> Fecha </InputLabel>
      <InputEdition value={sale.date} className="inputTextArea" inputProps={{disabled:id!=0}} onChange={(event)=>{
        setDate(event.target.value);
      }} />
      </div>
      )
    }
  }
  function CartOld(){
    if(!changeCart){
    return(
      <Card className={classesCard.root} variant="outlined">
      <CardHeader className={classesCard.title}
      title="Carrito"
      />
      <CardContent>
          {oldCart.map((item)=>{
            return <CustomCardRecipe title={item.name} description={item.description} quantity={item.quantity}></CustomCardRecipe>
          })}
      </CardContent>
      </Card>
    )
    }else{
      return <div></div>;
    }
  }
    return (
      <div>
        <div className="productsEdition" >
          <div className = "inputsEdition">
            <InputLabel> Nombre del Comprador</InputLabel>
            <InputEdition  value={sale.userInfo.name} className="inputTextBox" inputProps={{disabled:id!=0, placeholder: "Nombre del Comprador"}} onChange={(event)=>{
                setUserInfo({...userInfo, name:event.target.value});
              }}></InputEdition>
          </div>
          <div className = "inputsEdition">
            <InputLabel> Apellido del Comprador</InputLabel>
            <InputEdition  value={sale.userInfo.surname} className="inputTextBox" inputProps={{disabled:id!=0, placeholder: "Apellido del Comprador"}} onChange={(event)=>{
                setUserInfo({...userInfo, surname:event.target.value});
              }}></InputEdition>
          </div>
          <div className = "inputsEdition">
            <InputLabel> Email </InputLabel>
            <InputEdition value={sale.userInfo.email} className="inputTextBox" inputProps={{disabled:id!=0, placeholder: "Email"}} onChange={(event)=>{
                setUserInfo({...userInfo, email:event.target.value});
              }}></InputEdition>
          </div>
          <div className = "inputsEdition">
            <InputLabel> Celular </InputLabel>
            <InputEdition type="number" value={sale.userInfo.phone} className="inputTextBox" inputProps={{disabled:id!=0, placeholder: "Celular"}} onChange={(event)=>{
                setUserInfo({...userInfo, phone:event.target.value});
              }}></InputEdition>
          </div>
          {dateInput()}
          <div className = "textAreaEdition">
          <InputLabel> Descripción </InputLabel>
          <InputEdition value={sale.userInfo.description} className="inputTextArea" inputProps={{disabled:id!=0, placeholder: "Descripción"}} onChange={(event)=>{
                setDescription(event.target.value);
          }} />
          </div>

          <hr className = "titleHR"></hr>
          <div className="inputsEdition">
            <InputLabel >Carrito de Compra</InputLabel>
            <Select
              inputProps={{disabled:id!=0}}
              multiple
              value={productList}
              onChange={handleChange}
              input={<InputChip/>}
              renderValue={(selected) => (
                <div className={classes.chips}>
                  
                  {selected.map((value) =>
                    <Chip key={value.id} label={value.name} className={classes.chip} />
                  )}
                </div>
              )}
              MenuProps={MenuProps}
            >
              {fullList.map((name) => (
                <MenuItem
                  key={name.id}
                  value={name}
                >
                  {name.name}
                </MenuItem>
              ))}
            </Select>
          </div>
          <div className = "titleHR"></div>
        {productList.map((item)=>{
          return <CustomCard quantity={item.quantity} handelQuantityChange={handelQuantityChange} title={item.name} pId={item.id} description={item.description} handleMaterialChange={getListAction}></CustomCard>
        })} 
        
        <CartOld></CartOld>

        <div className="dummySpaceForButton"></div>
        <CancelButton color="secondary" size="large" className="cancelButton" onClick={()=>{
         history.push("/Sales");
        }}> Cancelar </CancelButton>
        <CustomButton disabled={id!=0} className="buttonSave" variant="outlined" color="default" size="large" onClick={()=>{
         save();
        }}> Guardar </CustomButton>
        </div>
      </div>
    )
}
