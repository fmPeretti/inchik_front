
import React, { useState, useEffect } from 'react'
import "../../Admin.css"
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { purple } from '@material-ui/core/colors';
import Axios from 'axios'
import TableForm from "../../components/TableForm.js"
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { red } from '@material-ui/core/colors';
import CustomButton from "../../components/CustomButton"

const TextFieldCustom = withStyles((theme) => ({
  root: {
    backgroundColor: purple[0],
    '&:hover': {
      
    },
    '& label.Mui-focused': {
      color: purple[600],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: purple[800],
    },
  },
}))(TextField);


const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));
export default function Materials() {
  
  useEffect(() => {
    Axios.get("http://localhost:3001/search/materials").then((response)=>{
      setFullList(response.data);
      setMaterialList(response.data);
    })
  }, []);
  
  const titleAdmin = "Administración de Materiales de Producción";
  const [fullList, setFullList] = useState([]);
  const [materialList, setMaterialList] = useState([]);
  const tableColumns = [
    {label:"Id de Material", id:"id",  minWidth: 40 },
    {label:"Nombre", id:"name",  minWidth: 120 },
    {label:"Costo", id:"cost", minWidth: 100},
  ];
  const classes = useStyles();
  return (
    
        <form noValidate autoComplete="off">
        <div className="adminMain">
          <h1 className="titleAdmin"> {titleAdmin}</h1>
          <hr className="titleHR"></hr>
          <TextFieldCustom className="inputField" id="standard-basic" label="Nombre" onChange={(event)=>{
            setMaterialList( event.target.value=== "" ? fullList : fullList.filter(item => item.name.toLowerCase().includes(event.target.value.toLowerCase())))
          }}/>
          <div className="tableSpan">
          <TableForm tableData={materialList} tableColumns={tableColumns} type="materials"> </TableForm>
          </div>

          <CustomButton className="addButton" href="/materialsEdition/0" variant="outlined" color="default" size="large"> Agregar +</CustomButton>          
        </div>
        </form>
    )
}