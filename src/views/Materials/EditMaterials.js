import React, {useState, useEffect, useRef} from 'react'; 
import "./Materials.css"
import Axios from "axios"
import InputEdition from "../../components/InputEdition"
import InputLabel from '@material-ui/core/InputLabel';
import { useParams } from 'react-router-dom';
import CustomButton from '../../components/CustomButton';
import CancelButton from '../../components/CancelButton';
import { useHistory } from 'react-router';

export default function EditMaterials() {

  const removeFromList = (array, index)=>{
    array.splice(index, 1);
    return array
  }

  useEffect(() => {
    if(id!=0){
      Axios.get("http://localhost:3001/retrieveMaterialById/", {params:{id:id}}).then((response)=>{
        setMaterial(response.data[0]);
      })
    }
  }, []);
  const save = ()=>{
    if(name!=null && name.length>0 && cost>0){
      Axios.post("http://localhost:3001/save/materials", {name:name, cost:cost, description:description, firstStock:firstStock}).
        then((response)=>{
          if(response.statusText==="OK"){
            alert("El Material se agregó correctamente");  
            history.push("/materials");
          }else alert( "El material no se Pudo agregar por el Error: " + response.data.statusText);
        })
    }else alert("Campos incompletos");  
  }
  const update = ()=>{
    if(material.name.length>0 && material.cost>0){
      Axios.post("http://localhost:3001/update/material", material).
        then((response)=>{
          if(response.statusText==="OK"){
            alert("El Material se guardó correctamente");  
            history.push("/materials");
          }else alert("El material no se Pudo agregar por el Error: " + response.data.statusText);
        })
    }else alert("Campos incompletos");  
}
  const [firstStock, setFirstStock] = useState(0);
  const history = useHistory();
  const [material, setMaterial] = useState({});
  const { id } = useParams();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [cost, setCost] = useState(0);
  const [request, setRequest] = useState({name:"", description:"", recipeId:0, price:0});
  function defaultStock(){
    if(id==0){
      return(
        <div className = "inputsEdition">
          <InputLabel> Stock Inicial </InputLabel>
          <InputEdition  className="inputTextBox" inputProps={{placeholder: "Stock Inicial"}} onChange={(event)=>{
                setFirstStock(event.target.value);
            }}></InputEdition>
        </div>
      )
    }
  };  
  return (
      <div>
        <div className="materialsEdition" >
          <div className = "inputsEdition">
            <InputLabel> Nombre </InputLabel>
            <InputEdition  disabled={id!=0} value={material.name} className="inputTextBox" inputProps={{placeholder: "Nombre"}} onChange={(event)=>{
                  setName(event.target.value);
                  setMaterial({...material, name: event.target.value});
              }}></InputEdition>
          </div>
          <div className = "inputsEdition">
            <InputLabel> Costo </InputLabel>
            <InputEdition value={material.cost} className="inputTextBox" inputProps={{placeholder: "Costo", type: "number"}} onChange={(event)=>{
              setCost(event.target.value);
              setMaterial({...material, cost: event.target.value});
            }} />
          </div>
          <div className = "textAreaEdition">
          <InputLabel> Descripción </InputLabel>
          <InputEdition value={material.description} className="inputTextArea" inputProps={{placeholder: "Descripción"}} onChange={(event)=>{
                setDescription(event.target.value);
                setMaterial({...material, description: event.target.value});
            }} />
          </div>

          {defaultStock()}
          
        <div className="dummySpaceForButton"></div>
        <CancelButton color="secondary" size="large" className="cancelButton" onClick={()=>{
         history.push("/Materials");
        }}> Cancelar </CancelButton>
        <CustomButton className="saveButton" variant="outlined" color="default" size="large" onClick={()=>{
         id==0 ? save() : update();
        }}> Guardar </CustomButton>

        

        </div>
      </div>
    )
}
