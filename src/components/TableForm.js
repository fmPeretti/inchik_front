import React, {useState} from 'react';
import Axios from 'axios';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { red } from '@material-ui/core/colors';
import ConfirmDialog from './ConfirmDialog'
import { useHistory } from 'react-router'

 const DeleteIconStyled = withStyles((theme) => ({
   root: {
     '&:hover': {
       color:red[500],
     },
   },
 }))(DeleteIcon);
function createData(name, code, population, size) {
  const density = population / size;
  return { name, code, population, size, density };
}
let columns = [];
let rows = [];
const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 1000,
  },
});

export default function TableForm(props) {
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [idToDelete, setIdToDelete] = useState(false);
  const [nameToDelete, setNameToDelete] = useState(false);
  const deleteMethod = (id) =>{
    setIdToDelete(null);
    Axios.post("http://localhost:3001/delete/"+props.type, {id:id, name:nameToDelete}).then((response)=>{
      setNameToDelete(null);
      history.go(0);
  })
  };
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  columns = props.tableColumns;
  rows = props.tableData!=[] ? props.tableData : rows;
    return (
    <div className="tableFormStyle">
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
              <TableCell> </TableCell>
            </TableRow>
            
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                  <TableCell>
                       <IconButton aria-label="edit" size="small" href={"/"+props.type+"Edition/"+row.id} ><EditIcon  fontSize="small" /></IconButton>  
                       <IconButton aria-label="delete" size="small" onClick={()=>{setOpen(true); setIdToDelete(row.id); setNameToDelete(row.name); }}><DeleteIconStyled fontSize="small" /></IconButton> 
                    </TableCell>
                  
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
      />
    </Paper>
      <ConfirmDialog
        title="Eliminar"
        open={open}
        setOpen={setOpen}
        onConfirm={deleteMethod}
        id={idToDelete}
      >
        ¿Estás seguro de que deseas Eliminar este item?
      </ConfirmDialog>

    </div>
  );
}
