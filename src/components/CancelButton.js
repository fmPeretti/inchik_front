import {
  fade,
  withStyles,
} from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import { purple } from '@material-ui/core/colors';
import Button from '@material-ui/core/Button';

const CancelButton = withStyles((theme) => ({
  root: {
    height:50,
    fontWeight:'bolder',
    color: "white",
    backgroundColor: "#f50057",
    '&:hover': {
      backgroundColor: "#ab184c",
      color:purple[50],
    },
  },
}))(Button);

  export default CancelButton;