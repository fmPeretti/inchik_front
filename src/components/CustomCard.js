import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import InputEdition from "./InputEdition"
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles({
  root: {
    minWidth: 300,
    minHeight: 150,
    background: '#d689e4',
  },
  bullet: {
    margin: '0 2px',
    color: "white",
    fontSize: 20,
  },
  title: {
    color: "white",
  },
  pos: {
    marginBottom:0,
    color: "white",
    fontSize: 15,
  },
  posLabel: {
    paddingTop: "7%",
    paddingBottom: "2%",
    color: "white",
    fontSize: 15,
  }
});

export default function CustomCard(props) {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <div className="cardsEdition">
    <Card className={classes.root} variant="outlined">
      <CardHeader className={classes.title}
        action={
          <IconButton className = {classes.title} aria-label="settings" onClick={(event)=>{props.handleMaterialChange(props.pId)}}>
            <CloseIcon />
          </IconButton>
        }
        title={props.title}
      />
      <CardContent>
        <Typography className={classes.bullet}>
        <InputLabel className={classes.bullet}> Cantidad </InputLabel>
            <InputEdition value={props.quantity} className="inputTextBox" inputProps={{placeholder: "Cantidad", type: "number"}} 
            onChange={(event)=>{
              props.handelQuantityChange(event.target.value, props.pId);
            }}/>
        </Typography>
        <Typography className={classes.pos}>
          <InputLabel className = {classes.posLabel} >Descripción:</InputLabel>
          {props.description}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small"></Button>
      </CardActions>
    </Card>
    </div>
  );
}
