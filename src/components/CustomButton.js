import {
  fade,
  withStyles,
} from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import { purple } from '@material-ui/core/colors';
import Button from '@material-ui/core/Button';

const CustomButton = withStyles((theme) => ({
  root: {
    height:50,
    fontWeight:'bolder',
    color: purple[50],
    backgroundColor: purple[400],
    '&:hover': {
      backgroundColor: purple[500],
      color:purple[50],
    },
  },
}))(Button);

  export default CustomButton;