import React, {useContext} from 'react'
import "../App.css"
import logotipo from "../isotipo.svg"
import Button from '@material-ui/core/Button';  
import Badge from '@material-ui/core/Badge';
import Context from "../store/context"
export default function TopBar({currentView}) {
    
    const {state} = useContext(Context);
    
    return (
        <div>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"></link>
        
        <nav className="navbar sticky-top navbar-light inchikPinkBg topBar">
            <div className="">
                <a className="navbar-brand" href="/">
                <img src={logotipo} alt="" className="navBarIso"/>
                </a>

                <a className="navbar-brand" href="/products">    
                    <Button variant="outlined" color="default" size="large">
                        PRODUCTOS             
                    </Button> 
                </a>

                <a className="navbar-brand" href="/materials">      
                    <Button variant="outlined" color="default" size="large">
                        MATERIALES           
                    </Button>  
                </a>


                <a className="navbar-brand" href="/stocks"> 
                    <Button variant="outlined" color="default" size="large">
                        STOCKS              
                    </Button>                
                </a>

                <a className="navbar-brand" href="/sales">  
                    <Button variant="outlined" color="default" size="large">
                        VENTAS         
                    </Button>       
                </a>

                <a className="navbar-brand" href="/logs">  
                    <Badge badgeContent={state.messagesQuantity} max={10} color="secondary">
                    <Button variant="outlined" color="default" size="large">
                        MENSAJES               
                    </Button>       
                    </Badge>
                </a>
            </div>
        </nav>
        </div>
    )
}
