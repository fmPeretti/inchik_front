import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import InputEdition from "./InputEdition"
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles({
  root: {
    minWidth: 300,
    minHeight: 150,
    background:'#b93b6e',
    margin:10,
  },
  bullet: {
    margin: '0 2px',
    color: "white",
    fontSize: 20,
  },
  title: {
    color: "white",
  },
  pos: {
    marginBottom:0,
    color: "white",
    fontSize: 15,
  },
  posLabel: {
    color: "white",
    fontSize: 15,
  }
});

export default function CustomCardRecipe(props) {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <div className="cardsEdition">
    <Card className={classes.root} variant="outlined">
    <CardHeader className={classes.title}
        title={props.title}
      />
      <CardContent>
        <Typography className={classes.bullet}>
            <h5  className="inputTextBox"> Cantidad : {props.quantity}</h5>
        </Typography>
        <Typography className={classes.pos}>
          <InputLabel className = {classes.posLabel} >Descripción:</InputLabel>
          {props.description}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small"></Button>
      </CardActions>
    </Card>
    </div>
  );
}
